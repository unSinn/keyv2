        // entry point for customizer script. This probably isn't useful to most people,
// as it's just a wrapper that helps generate customizer.scad for thingiverse.

/* [Basic-Settings] */

// what preset profile do you wish to use? disable if you are going to set paramters below
//key_profile = "dsa"; // [dcs, oem, dsa, sa, g20, disable] // wird bei Taste selber gesteuert

include <src/settings.scad>

include <src/key_sizes.scad>
include <src/key_profiles.scad>
include <src/key_types.scad>
include <src/key_transformations.scad>
include <src/key_helpers.scad>

use <src/key.scad>

//$font="Noto Sans:style=Bold";
//$font="Noto Sans:style=Black";
$font="Nimbus Sans:style=Bold";
//$font="Cantarell:style=ExtraBold";
//$font="URW Gothic:style=Demi";
//$font="DejaVu Sans Mono:style=Demi";


translate_u(0,0) key_profile("g20", 1) legend("1+|",size=4.5) {
  key(inset=true);
}


translate_u(1,0) key_profile("sa", 1) legend("2\"@",size=4.5) {
  key(inset=true);
}

translate_u(2,0) key_profile("dsa", 1) legend("3*#",size=4.5) {
  key(inset=true);
}

translate_u(0,-1) key_profile("g20", 2) legend("Q",size=4.5) {
  key(inset=true);
}

translate_u(1,-1) key_profile("sa", 2) legend("W▲",size=4.5) {
  key(inset=true);
}
translate_u(2,-1) key_profile("dsa", 2) legend("E",size=4.5) {
  key(inset=true);
}
