// entry point for customizer script. This probably isn't useful to most people,
// as it's just a wrapper that helps generate customizer.scad for thingiverse.

/* [Basic-Settings] */

// what preset profile do you wish to use? disable if you are going to set paramters below
key_profile = "oem"; // [dcs, oem, dsa, sa, g20, disable]
// what key profile row is this keycap on? 0 for disable

include <src/settings.scad>

include <src/key_sizes.scad>
include <src/key_profiles.scad>
include <src/key_types.scad>
include <src/key_transformations.scad>
include <src/key_helpers.scad>

use <src/key.scad>

translate_u(-1,1) key_profile(key_profile, 2) legend("Q") {
  key(inset=true);
}

translate_u(0,1) key_profile(key_profile, 2) legend("W") {
  key(inset=true);
}

translate_u(-1,0) key_profile(key_profile, 3) legend("A") {
  key(inset=true);
}

translate_u(0,0) key_profile(key_profile, 3) legend("S") {
  key(inset=true);
}

translate_u(1,0) key_profile(key_profile, 3) legend("D") {
  key(inset=true);
}

translate_u(0,-2) key_profile(key_profile, 3) {
  5_75u() spacebar() key(inset=true);
}

translate_u(-2,-1) key_profile(key_profile, 4) legend("SHIFT", size=3) {
  1_2u() key(inset=true);
}
