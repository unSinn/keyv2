/*use <key.scad>*/
// NEED to include, not use this, even with the default variables set. don't know why
include <keys.scad>

// Font used for text
//$font="DejaVu Sans Mono:style=Bold";
//$font="Noto Sans Display:style=Black"; //keine ASCII Pfeile
//$font="URW Gothic:style=Demi";
$font="Nimbus Sans:style=Bold";
key_profile = "dcs";


60_percent = [
  [1,1,1,1,1,1,1,1,1,1,1,1,1,2],
  [1.5,1,1,1,1,1,1,1,1,1,1,1,1,1.5],
  [1.75,1,1,1,1,1,1,1,1,1,1,1,1,1.25],
  [1.25,1,1,1,1,1,1,1,1,1,1,1,1.75,1],
  [1.25,1.25,1.25,6.25,1.25,1.25,1.25,1.25]
];

beschriftung = [
  ["ESC","1+|","2\"@","3*#","4ç","5%","6&","7/","8(","9)","0=","?'´","^`~", "BACK"], 
   //"§°" auf Layer legen
  ["TAB","Q","W▲","E","R","T","Z","U","I","O","P","Üè[","¨!]","ENTER"],
  ["CAPS","◄A","S▼","D►","F","G","H","J","K","L","Öé","Äà{","$£}", "ENTER"],
  ["SHFT","<>\\","Y","X","C","V","B","N","M","\,;","\.:","-_","SHIFT","DEL"],
  ["CTRL","SUPA","ALT","","AGR","SUPA","CTRL","FN",]
];


function sum(list, x=0) =
  len(list) <= 1 ?
    x + list[0] :
    sum([for (x = [1: len(list) - 1]) list[x]], x+list[0]);

translate_u(6.9,-4) key_profile(key_profile, 3) {
  6_25u() spacebar() key();
}

for (row = [0:len(60_percent)-1]){
  for(column = [0:len(60_percent[row])-1]) {
    columnDist = sum([for (x = [0 : column]) 60_percent[row][x]]);
    a = 60_percent[row][column];
    translate_u(columnDist - (a/2), -row) g20_row(3) u(a) cherry() { // (row+4) % 5 + 1
      if (a == 6.25) {
        //spacebar() key();
      } else if (a == 2.25) {
        lshift() key(inset=true);
      } else if (a == 2) {
          legend(beschriftung[row][column], size=4.5)  {
                backspace() key(inset=true);
            }  
//      } else if (a == 1.5) {
//         legend(beschriftung[row][column], size=4.5)  {
//                iso_enter() key(inset=true);
//            }     
            
      } else if (a == 1.75) {
          legend(beschriftung[row][column], size=4.5)  {
                stepped_caps_lock() key(inset=true);
        }   
 //SteppedCapslock funktioniert irgendwie noch nicht richtig...
        
      } else if (a == 2.75) {
        rshift() key(inset=true);
      } else {
        legend(beschriftung[row][column], size=4.5)  {
                key(inset=true); 
            }
      }
    }
  }
}
