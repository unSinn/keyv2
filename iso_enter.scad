/*use <key.scad>*/
// NEED to include, not use this, even with the default variables set. don't know why
include <keys.scad>


// Font used for text
//$font="DejaVu Sans Mono:style=Bold";
//$font="Noto Sans Display:style=Black"; //keine ASCII Pfeile
//$font="URW Gothic:style=Demi";
$font="Nimbus Sans:style=Bold";
key_profile = "dcs";

module stl() translate([0,0,-1.5]) translate_u(12.3,-3.2) rotate(a=[0,-1,-90]) difference(){
             translate([0,0,-1]) minkowski()
                {
                    import("ISO-ENTER.stl", convexity=3);
                  sphere(r=0.6, $fn=10);
                }
            translate([-48,31,-1]) cube([31,18,7]);
}

60_percent = [
  [1,1,1,1,1,1,1,1,1,1,1,1,1,2],
  [1.5,1,1,1,1,1,1,1,1,1,1,1,1,1.5],
  [1.75,1,1,1,1,1,1,1,1,1,1,1,1,1.25],
  [1.25,1,1,1,1,1,1,1,1,1,1,1,1.75,1], 
  [1.25,1.25,1.25,6.25,1.25,1.25,1.25,1.25]
];

beschriftung = [
  ["ESC","1+|","2\"@","3*#","4ç","5%","6&","7/","8(","9)","0=","?'´","^`~", "BACK"], 
   //"§°" auf Layer legen
  ["TAB","Q","W▲","E","R","T","Z","U","I","O","P","Üè[","¨!]","DEL"],
  ["CAPS","◄A","S▼","D►","F","G","H","J","K","L","Öé","Äà{","$£}", "ENT"],
  ["SHFT","<>\\","Y","X","C","V","B","N","M","\,;","\.:","-_","SHIFT","§°"],
  ["CTRL","SUPA","ALT","","AGR","SUPA","CTRL","FN",]
];


function sum(list, x=0) =
  len(list) <= 1 ?
    x + list[0] :
    sum([for (x = [1: len(list) - 1]) list[x]], x+list[0]);
    
difference(){
    stl();
    translate([-500,-500,-100]) cube([1000,1000,100]);
}

for (row = [0:3]){
  for(column = [12:len(60_percent[row])-1]) {
    columnDist = sum([for (x = [0 : column]) 60_percent[row][x]]);
    a = 60_percent[row][column];
    translate_u(columnDist - (a/2), -row) g20_row(3) u(a) cherry() { // (row+4) % 5 + 1
      if (a == 6.25) {
        //spacebar() key();
      } else if (row==2 && column== 13) {
          
      }
          else if (row==1 && column== 13) {
              legend(beschriftung[row][column], size=4.5)  {
                translate_u(0.15 , -0.5) iso_enter() key(inset=true);
            }     
              }
              else if (a == 2.25) {
              }
        
       else if (a == 2) {
          legend(beschriftung[row][column], size=4.5)  {
                backspace() key(inset=true);
            }  
      } else if (a == 1.5) {
        
            
      } else if (a == 1.75) {
          legend(beschriftung[row][column], size=4.5)  {
                stepped_caps_lock() key(inset=true);
        }   
 //SteppedCapslock funktioniert irgendwie noch nicht richtig...
        
      } else if (a == 2.75) {
        rshift() key(inset=true);
      } else {
        legend(beschriftung[row][column], size=4.5)  {
                key(inset=true); 
            }
      }
    }
  }
}
